package main

import (
	"crypto/sha256"
	"encoding/hex"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/syndtr/goleveldb/leveldb"
)

// Storing the DB globally since its concurrent and threadsafe,
// along with access from non-main functions without passing it around.
var db *leveldb.DB

func main() {
	ldb, err := leveldb.OpenFile("rigetti.db", nil)
	if err != nil {
		log.WithField("error", err).Fatal("Could not open database.")
	}
	db = ldb
	defer db.Close()

	router := gin.Default()
	router.StaticFile("/", "./index.html")
	api := router.Group("/api")
	{
		api.GET("/message/:hash", getMessage)
		api.POST("/message", saveMessage)
	}
	router.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

// Used a single struct for both functions to minimize verbosity.
// Depending on complexity of the application, this can be split into many structs for each API call.
type MessageData struct {
	Text string `json:"text"`
	Hash string `json:"hash"`
}

func getMessage(c *gin.Context) {
	hash := c.Param("hash")
	message, err := db.Get([]byte(hash), nil)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Could not find the hash provided."})
		return
	}
	resp := MessageData{Text: string(message)}
	c.JSON(http.StatusOK, resp)
}

func saveMessage(c *gin.Context) {
	var json MessageData

	err := c.ShouldBindJSON(&json)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if json.Text == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Did not provide any text."})
		return
	}

	hash := sha256.Sum256([]byte(json.Text))
	hashString := hex.EncodeToString(hash[:])
	err = db.Put([]byte(hashString), []byte(json.Text), nil)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	resp := MessageData{Hash: hashString}
	c.JSON(http.StatusOK, resp)
}
