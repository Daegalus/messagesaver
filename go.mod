module rigetti-takehome

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/sirupsen/logrus v1.8.1
	github.com/syndtr/goleveldb v1.0.0
)
