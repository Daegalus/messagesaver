FROM golang:1.16-alpine AS builder
WORKDIR /root/
COPY main.go .
COPY go.mod .
COPY go.sum .
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-extldflags=-static" -o rigetti main.go 

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /root/rigetti .
COPY index.html .
CMD ["./rigetti"]  