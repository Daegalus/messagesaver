## How to run it

You can clone this repo, and run `go run main.go` and access `http://localhost:8080` in your browser.

You can also build the docker image using `docker build -t messagesaver .` and then running it `docker run -d -p 8080:8080 --name messagesaver messagesaver` and go to `http://localhost:8080` in your browser.

If you wish to persist the data between containers, add `-v your/path/here:/root/rigetti.db` to your docker run so it is saved on your drive.

## Structure

### k8s/

This just has the k8s manifest files to run the service on a local k8s cluster I have. It is somewhat specific to my setup, and uses hostpath mounting to store persistant data on a disk.

### scripts/

Just a few docker scripts so I don't typo or forget what I called the docker image.

### index.html

Page served when you hit "/" and has a lightweight JS based form to save/retrieve the api.


## API

#### GET /api/messages/:hash

This retrieves the message at the hash. If it doesn't exist it will return a 404 with a error json body.

Response:

```json
{
    "hash": "",
    "text": "The returned message"
}
```

#### POST /api/messages

This will save the provided message and return a SHA256 hash to later retrieve the message.

Request:
```json
{
    "text": "The returned message"
}
```

Response:

```json
{
    "hash": "912198adfefa765e45af865f6df8",
    "text": ""
}
```

## Database

A simple key value store is sufficient for this. To minimize deployment overhead, used a pure go LevelDB implementation. Supports concurrent access and usage, and saves to a local file. When in docker, a host path is mounted into the container to store the db.


## Dockerfile

Used a multi-stage docker file so that it is built in a dedicated container, then the resulting binary is copied to the actual resulting fresh docker image, so that sources are not in the docker image and build tools are absent.